import SideNav from '@/app/ui/dashboard/sidenav';

function Layout({ children }: { children: React.ReactNode }) {
  return (
    <div className="md:over flex h-screen flex-col md:flex-row md:overflow-hidden">
      <div className="w-full flex-none md:w-64">
        <SideNav></SideNav>
      </div>
      <div className="md:overflow-hidden-y-auto flex-grow p-6 md:p-12">
        {children}
      </div>
    </div>
  );
}

export default Layout;
